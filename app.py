from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_marshmallow import Marshmallow
#from config import config

db = SQLAlchemy()
ma = Marshmallow()

def create_app():
    app = Flask(__name__)
    app.config.from_object('config.DevelopmentConfig')
    #app.config.from_object('config.ProductionConfig')
    db.init_app(app)
    ma.init_app(app)

    with app.app_context():
        # Migrations
        from src.hola.schema.inicio_schema import HomeSchema
        from src.hola.model.inicio import Home
        db.create_all()
        
        # Imports
        from src.hola.route.hola import hola_blueprint
        # Register Blueprints
        app.register_blueprint(hola_blueprint)
        
        
        return app


  