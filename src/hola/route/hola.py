from flask import Blueprint, request, jsonify, make_response
from src.hola.schema import inicio_schema 
from src.hola.model.inicio import Home 
from app import db,ma

hola_blueprint = Blueprint('hola', __name__)

@hola_blueprint.route("/")
def hola():
    return "Hello World!"



@hola_blueprint.route("/vctr")
def hola_vctr():
    return "Hello vctr!"

@hola_blueprint.route('/vctr/list', methods = ['GET'])
def list():
    get_home_s = Home.query.all()
    home_schema = inicio_schema.HomeSchema(many=True)
    lista = home_schema.dump(get_home_s)
    return make_response(jsonify({"home": lista}))

@hola_blueprint.route('/vctr/<id>', methods = ['GET'])
def get_inicio_by_id(id):
    get_inicio = Home.query.get(id)
    home_schema = inicio_schema.HomeSchema()
    inicio_one = home_schema.dump(get_inicio)
    return make_response(jsonify({"inicio": inicio_one}))

@hola_blueprint.route('/vctr/register', methods = ['POST'])
def create_inicio():
    data = request.get_json()
    home_schema = inicio_schema.HomeSchema()
    inicio = home_schema.load(data)
    inicio.save()
    result = home_schema.dump(inicio)
    return make_response(jsonify({"home": result}),200)

@hola_blueprint.route('/vctr/update/<id>', methods = ['PUT'])
def update_inicio_by_id(id):
    data = request.get_json()
    get_inicio = Home.query.get(id)
    if data.get('id'):
        get_inicio.id = data['id']
    if data.get('descripcion'):
        get_inicio.descripcion = data['descripcion']    
    home_schema = inicio_schema.HomeSchema(only=['id', 'descripcion'])
    inicio = home_schema.dump(get_inicio.update())
    return make_response(jsonify({"inicio": inicio}))